package hr.fer.diplomski.seminar_2.model

import java.io.Serializable

data class Card(
    var id: Long = 1L,
    var word: String,
    var translation: String
                ): Serializable