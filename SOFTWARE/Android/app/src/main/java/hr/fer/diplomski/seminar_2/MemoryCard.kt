package hr.fer.diplomski.seminar_2

data class MemoryCard(
    val word: String,
    var isFaceUp: Boolean = false,
    var isMatched: Boolean = false
)
