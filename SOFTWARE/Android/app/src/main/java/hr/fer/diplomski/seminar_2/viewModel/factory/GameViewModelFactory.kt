package hr.fer.diplomski.seminar_2.viewModel.factory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import hr.fer.diplomski.seminar_2.database.DatabaseHandler
import hr.fer.diplomski.seminar_2.viewModel.GameViewModel

class GameViewModelFactory(private val databaseHandler: DatabaseHandler) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return GameViewModel(databaseHandler) as T
    }


}