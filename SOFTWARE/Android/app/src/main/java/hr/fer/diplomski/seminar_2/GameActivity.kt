package hr.fer.diplomski.seminar_2

import android.os.Bundle
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import hr.fer.diplomski.seminar_2.database.DatabaseHandler
import hr.fer.diplomski.seminar_2.databinding.ActivityGameBinding
import hr.fer.diplomski.seminar_2.model.Game
import hr.fer.diplomski.seminar_2.viewModel.GameViewModel
import hr.fer.diplomski.seminar_2.viewModel.factory.GameViewModelFactory

class GameActivity : AppCompatActivity() {

    private lateinit var binding: ActivityGameBinding
    private lateinit var buttons: List<Button>
    private lateinit var cards: List<MemoryCard>
    private var indexOfSingleSelectedCard: Int? = null
    private lateinit var viewModel: GameViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityGameBinding.inflate(layoutInflater)
        setContentView(binding.root)

        supportActionBar?.title = "Match word with translation"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        binding.exitButton.setOnClickListener {
            finish()
        }

        val databaseHandler: DatabaseHandler = DatabaseHandler(applicationContext)
        val viewModelFactory = GameViewModelFactory(databaseHandler = databaseHandler)
        viewModel = ViewModelProvider(this, viewModelFactory).get(GameViewModel::class.java)

        val game: Game = intent.getSerializableExtra("game") as Game
        viewModel.getCards(gameId = game.id)

        val words = mutableListOf<String>()
        viewModel.cards.value?.cards?.forEach {
            words.add(it.word)
            words.add(it.translation)
        }
        words.shuffle()

        buttons = listOf(binding.imageButton1, binding.imageButton2, binding.imageButton3, binding.imageButton4, binding.imageButton5,
            binding.imageButton6, binding.imageButton7, binding.imageButton8)

        cards = buttons.indices.map { index ->
            MemoryCard(words[index])
        }

        buttons.forEachIndexed { index, button ->
            button.setOnClickListener {
                button.animate().apply {
                    duration = 500
                    rotationYBy(360f)

                }.withEndAction {
                    updateModels(index)
                    updateViews()
                }
            }
        }
    }

    private fun updateViews() {
        cards.forEachIndexed { index, card ->
            val button = buttons[index]
            if (card.isMatched) {
                button.isEnabled = false
            }
            if (card.isFaceUp) {
                button.text = card.word
            } else {
                button.text = ""
            }
        }
    }

    private fun updateModels(position: Int) {
        val card = cards[position]
        if (card.isFaceUp) {
            Toast.makeText(this, "Invalid move!", Toast.LENGTH_SHORT).show()
            return
        }

        if (indexOfSingleSelectedCard == null) {
            restoreCards()
            indexOfSingleSelectedCard = position

        } else {
            checkForMatch(indexOfSingleSelectedCard!!, position)
            indexOfSingleSelectedCard = null
        }
        card.isFaceUp = !card.isFaceUp
    }

    private fun restoreCards() {
        cards.forEachIndexed { index, card ->
            val button = buttons[index]
            if (!card.isMatched) {

                if (card.isFaceUp) {
                    button.animate().apply {
                        rotationYBy(360f)
                    }.start()
                }
                card.isFaceUp = false
            }
        }
    }

    private fun checkForMatch(position1: Int, position2: Int) {
        for (card in viewModel.cards.value?.cards!!) {
            if (cards[position1].word.equals(card.word)) {
                if (cards[position2].word.equals(card.translation)) {
                    Toast.makeText(this, "Match found!!", Toast.LENGTH_SHORT).show()
                    cards[position1].isMatched = true
                    cards[position2].isMatched = true
                    break
                }
            }
            if (cards[position1].word.equals(card.translation)) {
                if (cards[position2].word.equals(card.word)) {
                    Toast.makeText(this, "Match found!!", Toast.LENGTH_SHORT).show()
                    cards[position1].isMatched = true
                    cards[position2].isMatched = true
                    break
                }
            }
        }

    }
}